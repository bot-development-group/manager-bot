import telebot
import config
import os
from flask import Flask, request

bot = telebot.TeleBot(config.token)


@bot.message_handler(content_types=['document', 'photo', 'video'])
def delete_links_file(message):
    if message.caption_entities is not None and "group" in message.chat.type:
        for entity in message.caption_entities:
            if entity.type in ["url", "text_link"] and 'do you want sex' in message.caption.lower():
                bot.delete_message(message.chat.id, message.message_id)
                bot.kick_chat_member(message.chat.id, message.from_user.id, until_date=10)
                return
    else:
        return


@bot.message_handler(func=lambda message: message.entities is not None and "group" in message.chat.type)
def delete_links_text(message):
    for entity in message.entities:
        if entity.type in ["url", "text_link"] and 'do you want sex' in message.text.lower():
            bot.delete_message(message.chat.id, message.message_id)
            bot.kick_chat_member(message.chat.id, message.from_user.id, until_date=10)
            return


@bot.message_handler(func=lambda message: message.chat.type == 'private')
def send_contacts(message):
    bot.send_message(message.chat.id, config.contacts)


if __name__ == '__main__':
    bot.polling(none_stop=True)

    '''server = Flask(__name__)


    @server.route("/bot", methods=['POST'])
    def get_message():
        bot.process_new_updates([telebot.types.Update.de_json(request.stream.read().decode("utf-8"))])
        return "!", 200


    @server.route("/")
    def webhook():
        bot.remove_webhook()
        bot.set_webhook(
            url="https://mystery-house-bot.herokuapp.com/bot")
        return "?", 200


    server.run(host="0.0.0.0", port=os.environ.get('PORT', 80))'''
