# Manager Chat Bot

Бот для очистки чатов от спама.
Удаляет спам-сообщение и отправляет пользователя, отправившего это сообщение в бан.
***
Разработчик - *Castro Estelano*

> - GitHub - https://github.com/Heinlein1957
> - GitLab - https://gitlab.com/CastroEstelano